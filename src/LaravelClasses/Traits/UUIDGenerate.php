<?php

namespace LaravelClasses\Traits;


use Ramsey\Uuid\Uuid;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Schema;

trait UUIDGenerate
{
    /**
     * Boot the Uuid trait for the model.
     *
     * @return void
     */
    public static function bootUUIDGenerate()
    {
        static::creating(
            function ($model) {
                /** @var Model $model */
                if (Schema::connection($model->getConnectionName())->hasColumn($model->getTable(), 'reference')) {
                    $model->reference = (string)Uuid::uuid4();
                }
            });
    }

    /**
     * Find model by reference or fail.
     * @param $reference string Reference to look for.
     * @return mixed Return model.
     */
    public function referenceOrFail($reference)
    {
        return self::where('reference', $reference)->firstOrFail();
    }
}
