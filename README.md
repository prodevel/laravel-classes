#Laravel Classes
This file contains a collection of classes that can aid in the development of a project using Laravel Framework.

##Traits
* UUIDGenerate  
Generates new UUIDv4 references for eloquent models.  
Model table needs to have a column *reference*  
New function extends find or fail function to work with references.
We plan to make this a collection of providers so we can enable migrations for set tables on the fly.

##Documentation
Documentation for this library will be generated in due course.

##Support
Create a pull request with changes and we will look into it. You can also email support@prodevel.co.uk
